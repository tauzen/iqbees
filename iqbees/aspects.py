# -*- coding: utf-8 -*-
from collections import defaultdict
from enum import Enum

from iqbees.class_graph import ClassGraph
from iqbees.facts import get_entity_facts, get_entities_with_basic_aspect
from iqbees.settings import (
    SUPPORTED_RELATIONS,
    TOO_GENERAL_TYPES,
    TOO_GENERAL_RELATIONS
)


class AspectType(Enum):
    factual = 'factual'
    relational = 'relational'
    type = 'type'


class Aspect:
    def __init__(self, arg1, arg2, relation, type=AspectType.factual,
                 form=None):
        self.type = type
        self.arg1 = arg1
        self.arg2 = arg2
        self.relation = relation
        self.form = form if form else "{}({}, {})".format(relation, arg1, arg2)

    def __repr__(self):
        return self.form

    def __str__(self):
        return self.__repr__()

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return str(self) == str(other)

    def to_dict(self):
        return {
            'arg1': self.arg1,
            'arg2': self.arg2,
            'relation': self.relation,
            'form': self.form
        }

    @staticmethod
    def from_entity_fact(entity, fact):
        arg1 = '.' if entity == fact.arg1 else fact.arg1
        arg2 = '.' if entity == fact.arg2 else fact.arg2

        return Aspect(arg1, arg2, fact.relation)


class CompoundAspect:
    def __init__(self, aspects_set, entities=None):
        self._aspects = aspects_set
        self._entities = entities if entities else []

    def __repr__(self):
        return [a.form for a in list(self._aspects)]

    def __str__(self):
        return str(self.__repr__())

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return str(self) == str(other)

    @property
    def aspects(self):
        return self._aspects

    @property
    def entities(self):
        return self._entities

    def add_entity(self, entity):
        self._entities.append(entity)


def get_basic_aspects_shared_by_entities(entities, specific):
    shared_aspects = set()
    for entity in entities:
        basic_aspects = set(get_entity_basic_aspects(entity))
        if shared_aspects:
            shared_aspects.intersection_update(basic_aspects)
        else:
            shared_aspects = basic_aspects

    if specific:
        shared_type_aspects = set(
            filter(lambda f: f.relation == 'type', shared_aspects)
        )

        specific_type_aspects = _get_specific_type_aspects(shared_type_aspects)
        general_type_aspects = (
            shared_type_aspects.difference(specific_type_aspects)
        )

        shared_aspects.difference_update(general_type_aspects)

    return list(shared_aspects)


def get_entity_basic_aspects(entity, specific=False):
    facts = get_entity_facts(entity, SUPPORTED_RELATIONS)

    type_aspects = _get_type_aspects_from_facts(entity, facts)
    relational_aspects = _get_relational_aspects_from_facts(entity, facts)
    factual_aspects = _get_factual_aspects_from_facts(entity, facts)

    if specific:
        type_aspects = _get_specific_type_aspects(type_aspects)

    return type_aspects + relational_aspects + factual_aspects


def get_entities_with_shared_aspects(basic_aspects):
    """Retrieves dict of entities - aspects sets shared with basic_aspects"""
    result = defaultdict(set)
    for idx, aspect in enumerate(basic_aspects):
        entities = get_entities_with_basic_aspect(aspect)

        for e in entities:
            result[e].add(aspect)

    return result


def entities_aspects_to_compound_aspects(entities_aspects):
    compound_aspects = {}
    for entity, aspect_set in entities_aspects.items():
        ca = CompoundAspect(aspect_set)

        ca_key = str(ca)
        if ca_key not in compound_aspects:
            compound_aspects[ca_key] = ca

        compound_aspects[ca_key].add_entity(entity)

    return list(compound_aspects.values())


def _get_type_aspects_from_facts(entity, facts):
    type_facts = filter(
        lambda f: f.relation == 'type' and f.arg1 == entity,
        facts
    )

    type_aspects = map(
        lambda f: Aspect.from_entity_fact(entity, f),
        type_facts
    )

    return list(type_aspects)


def _get_factual_aspects_from_facts(entity, facts):
    non_type_facts = filter(lambda f: f.relation != 'type', facts)

    factual_aspects = map(
        lambda f: Aspect.from_entity_fact(entity, f),
        non_type_facts
    )

    return list(factual_aspects)


def _get_relational_aspects_from_facts(entity, facts):
    relations_count = defaultdict(int)
    for f in facts:
        if f.arg1 != entity or f.relation in TOO_GENERAL_RELATIONS:
            continue

        relations_count[f.relation] += 1

    relational_aspect = map(
        lambda relation: Aspect(
            arg1='.',
            arg2='?',
            relation=relation,
            type=AspectType.relational
        ),
        relations_count.keys()
    )

    return list(relational_aspect)


def _get_specific_type_aspects(aspects):
    all_types = set()
    all_superclasses = set()

    for aspect in aspects:
        clss = aspect.arg2
        all_types.add(clss)

        superclasses = ClassGraph().get_superclasses(clss)
        all_superclasses.update(set(superclasses))

    specific_types = all_types - all_superclasses - TOO_GENERAL_TYPES
    specific_aspects = filter(lambda a: a.arg2 in specific_types, aspects)

    return list(specific_aspects)
