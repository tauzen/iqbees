# -*- coding: utf-8 -*-
from iqbees import settings
from iqbees.data_access import db
from flask import Flask, jsonify, request


app = Flask(__name__)
app.debug = settings.DEBUG
app.config['SQLALCHEMY_DATABASE_URI'] = settings.DB_URI
app.config['SQLALCHEMY_ECHO'] = True
app.logger.info('App created, initialising DB connection')

db.init_app(app)
db.reflect(app=app)
app.logger.info('DB connection initialised')


# imported after DB init as we need to have table metadata available
from iqbees.aspects import (
    entities_aspects_to_compound_aspects,
    get_basic_aspects_shared_by_entities,
    get_entities_with_shared_aspects,
)
from iqbees.class_graph import ClassGraph
from iqbees.facts import (
    get_entity_facts,
    get_facts
)


class_graph = None
if settings.INIT_CLASS_GRAPH:
    with app.app_context():
        app.logger.info('Class graph init')
        class_graph = ClassGraph()
        app.logger.info(
            'Class entities: {}'.format(len(class_graph.get_all_classes()))
        )


@app.route('/')
def index():
    result = [
        fact.to_dict()
        for fact in get_facts()
    ]
    return jsonify(result)


@app.route('/classes/')
def show_class_list():
    if not class_graph:
        return jsonify([])

    try:
        page = int(request.args.get('page', 0))
    except ValueError:
        return "Bad Request", 400

    start = page * settings.CLASSES_PAGE_SIZE
    end = start + settings.CLASSES_PAGE_SIZE
    result = class_graph.get_all_classes()[start:end]

    return jsonify(result)


@app.route('/classes/<clss>')
def show_class_neighbours(clss):
    if not class_graph:
        return jsonify({})

    return jsonify({
        'superclasses': class_graph.get_superclasses(clss),
        'subclasses': class_graph.get_subclasses(clss)
    })


@app.route('/entities/<entity>')
def show_entity_facts(entity):
    relations = settings.SUPPORTED_RELATIONS

    rel_param = request.args.get('relations')
    if rel_param:
        relations = rel_param.split(',')

    entity_facts = get_entity_facts(entity, relations)

    result = {
        'count': len(entity_facts),
        'facts': [fact.to_dict() for fact in entity_facts],
    }
    return jsonify(result)


@app.route('/aspects/<entities>')
def show_entity_aspects(entities):
    specific = request.args.get('specific') and class_graph is not None
    entities = entities.split(',')

    basic_aspects = get_basic_aspects_shared_by_entities(entities, specific)
    app.logger.info(
        "Found {} aspects of {}".format(len(basic_aspects), entities)
    )

    result = [a.to_dict() for a in basic_aspects]
    return jsonify(result)


@app.route('/shared/<entities>')
def show_entities_with_shared_basic_aspects(entities):
    specific = request.args.get('specific') and class_graph is not None
    entities = entities.split(',')

    basic_aspects = get_basic_aspects_shared_by_entities(entities, specific)
    entities_shared_aspects = get_entities_with_shared_aspects(basic_aspects)

    return jsonify([
        {
            e: str(entities_shared_aspects[e])
        }
        for e in entities_shared_aspects.keys()
    ])


@app.route('/compound_aspects/<entities>')
def show_compound_aspects_with_sharing_entities(entities):
    details = request.args.get('details')
    specific = request.args.get('specific') and class_graph is not None
    entities = entities.split(',')

    basic_aspects = get_basic_aspects_shared_by_entities(entities, specific)
    entities_aspects = get_entities_with_shared_aspects(basic_aspects)

    for entity in entities:
        del entities_aspects[entity]
    compound_aspects = entities_aspects_to_compound_aspects(entities_aspects)

    app.logger.info(
        "Found {} compound aspects of {}".format(len(compound_aspects), entities)
    )

    result = {
        str(ca): ca.entities if details else len(ca.entities)
        for ca in compound_aspects
    }
    return jsonify(result)


if __name__ == '__main__':
    app.run(use_reloader=False)
