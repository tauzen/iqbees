# -*- coding: utf-8 -*-
from sqlalchemy import or_, and_

from iqbees.data_access import db
from iqbees.settings import SUBCLASS_RELATION


class Fact(db.Model):
    __table__ = db.Model.metadata.tables['facts']

    def __repr__(self):
        return '{}({},{})'.format(self.relation, self.arg1, self.arg2)

    def to_dict(self):
        return {
            'arg1': self.arg1,
            'arg2': self.arg2,
            'relation': self.relation
        }


def get_facts(limit=10):
    return Fact.query.limit(limit).all()


def get_entity_facts(entity, relations):
    return Fact.query.filter(
        and_(
            or_(Fact.arg1 == entity, Fact.arg2 == entity),
            Fact.relation.in_(relations)
        )
    ).order_by(Fact.relation).all()


def get_subclass_facts():
    return Fact.query.filter(Fact.relation == SUBCLASS_RELATION).all()


def get_entities_with_basic_aspect(aspect):
    conditions = [Fact.relation == aspect.relation]

    if aspect.arg1 == '.':
        column = Fact.arg1
        if aspect.arg2 != '?':
            conditions.append(Fact.arg2 == aspect.arg2)
    else:
        column = Fact.arg2
        if aspect.arg1 != '?':
            conditions.append(Fact.arg1 == aspect.arg1)

    facts = Fact.query.distinct(column).filter(and_(*conditions))

    entities = map(
        lambda f: f.arg1 if column == Fact.arg1 else f.arg2,
        facts
    )

    return list(entities)
