# -*- coding: utf-8 -*-
from collections import defaultdict
from functools import partial

from iqbees.facts import get_subclass_facts


class ClassGraph(object):
    """Simplified graph representation of 'type' relation

    Lists all entities (classes) in the graph. Allows to check superclasses and
    subclasses of specific class, Implemented as a Singleton.

    """

    _instance = None
    _initialised = False

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        if ClassGraph._initialised:
            return

        facts = get_subclass_facts()

        self._class_graph = defaultdict(partial(defaultdict, set))
        for f in facts:
            self._class_graph[f.arg1]['superclasses'].add(f.arg2)
            self._class_graph[f.arg2]['subclasses'].add(f.arg1)

        ClassGraph._initialised = True

    def get_superclasses(self, clss):
        return list(
            self._class_graph[clss]['superclasses']
            if clss in self._class_graph else set()
        )

    def get_subclasses(self, clss):
        return list(
            self._class_graph[clss]['subclasses']
            if clss in self._class_graph else set()
        )

    def get_all_classes(self):
        return list(self._class_graph.keys())
