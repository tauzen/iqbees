# -*- coding: utf-8 -*-
DEBUG = True
DB_URI = 'postgresql://krzysiek@localhost/qbees_db'

INIT_CLASS_GRAPH = True
CLASSES_PAGE_SIZE = 1000
SUBCLASS_RELATION = "subclassOf"

SUPPORTED_RELATIONS = [
    'wasCreatedOnDate',
    'occursUntil',
    'occursSince',
    'hasMusicalRole',
    'hasSuccessor',
    'actedIn',
    'type',
    'hasISBN',
    '_hasConfidence',
    'hasInflation',
    'hasDomain',
    'hasNumberOfWikipediaLinks',
    'using',
    'isCitizenOf',
    'wasFoundIn',
    'hasOfficialLanguage',
    'participatedIn',
    'inUnit',
    'hasValue',
    'endsExistingOnDate',
    'isPartOf',
    'hasExpenses',
    'hasGeoCoordinates',
    'hasThreeLetterLanguageCode',
    'hasPredecessor',
    'hasPoverty',
    'isAffiliatedTo',
    'isKnownFor',
    'hasGDP',
    'hasExternalWikipediaLinkTo',
    'hasProduct',
    'hasLongitude',
    'hasHeight',
    'startsExistingOnDate',
    'hasGini',
    'wasBornOnDate',
    'hasCapital',
    'happenedOnDate',
    'hasRange',
    '_timeToLocation',
    'isInterestedIn',
    'hasCurrency',
    'hasCitationTitle',
    'hasContext',
    'livesIn',
    'extractedOn',
    'hasPopulationDensity',
    'startedOnDate',
    'hasWeight',
    'hasAnchorText',
    'hasChild',
    'dealsWith',
    'hasWikipediaAbstract',
    'playsFor',
    'hasNumberOfPeople',
    'hasInternalWikipediaLinkTo',
    'hasSynsetId',
    'hasImport',
    'placedIn',
    'hasRevenue',
    'hasUTCOffset',
    'imports',
    'hasAcademicAdvisor',
    'hasGeonamesId',
    'hasLatitude',
    'created',
    'inLanguage',
    'hasUnemployment',
    'produced',
    'diedIn',
    'hasBudget',
    'diedOnDate',
    'hasICD10',
    'hasWebsite',
    'isMarriedTo',
    'worksAt',
    'holdsPoliticalPosition',
    'endedOnDate',
    'hasPages',
    'type_star',
    'hasArea',
    'exports',
    'isLeaderOf',
    'occursIn',
    'hasEconomicGrowth',
    'graduatedFrom',
    'isLocatedIn',
    'hasWikipediaArticleLength',
    'hasDuration',
    'influences',
    'hasImdb',
    'hasTitleText',
    'happenedIn',
    'hasWikipediaAnchorText',
    'wasDestroyedOnDate',
    'hasWikipediaCategory',
    'directed',
    'wasBornIn',
    'isPoliticianOf',
    'hasWonPrize',
    'hasTLD',
    'hasMotto',
    'hasHDI',
    'hasExport',
    'hasFamilyName',
    'hasGivenName',
    'hasPopulation',
]

TOO_GENERAL_TYPES = {
    'yagoGeoEntity',
    'wordnet_whole_100003553',
    'wordnet_group_100031264',
    'yagoPermanentlyLocatedEntity',
    'wordnet_peer_109626238',
    'wordnet_communication_100033020',
    'wordnet_causal_agent_100007347',
    'wordnet_unit_108189659',
    'yagoLegalActorGeo',
    'wordnet_abstraction_100002137',
    'wordnet_imagination_105625465',
    'wordnet_entity_100001740',
    'wordnet_artifact_100021939',
    'wordnet_organism_100004475',
    'wordnet_instrumentality_103575240',
    'wordnet_geographical_area_108574314',
    'wordnet_measure_100033615',
    'wordnet_object_100002684',
    'wordnet_adult_109605289',
    'wordnet_physical_entity_100001930',
    'wordnet_contestant_109613191',
    'wordnet_tract_108673395',
    'wordnet_relation_100031921',
    'yagoLegalActor',
    'wikicategory_Living_people'
}


# relational aspects of this relations will be ignored
TOO_GENERAL_RELATIONS = {
    'type',
    'wasBornOnDate',
    'wasBornIn',
    'hasFamilyName',
    'hasGivenName',
    'hasWebsite',
}