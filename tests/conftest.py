# -*- coding: utf-8 -*-
import os
import pytest


@pytest.fixture
def running_app_base_url():
    return "http://{host}:{port}/".format(
        host=os.environ['BASE_TEST_APP_HOST'],
        port=os.environ['BASE_TEST_APP_PORT'],
    )
