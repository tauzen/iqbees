# -*- coding: utf-8 -*-
import json
import pytest
import requests

from tests.v1_results import ENTITY, BASIC_ASPECTS, SPECIFIC_BASIC_ASPECTS


def test_service_responding(running_app_base_url):
    response = requests.get(running_app_base_url)
    assert response.status_code == 200


def test_basic_aspects_equals_v1(running_app_base_url):
    resp = requests.get(running_app_base_url + 'aspects/' + ENTITY)
    assert resp.status_code == 200

    aspects = json.loads(resp.text)
    result_set = _convert_aspects_to_v1_format(aspects)

    assert result_set == BASIC_ASPECTS


def test_specific_basic_aspects_equals_v1(running_app_base_url):
    resp = requests.get(
        running_app_base_url + 'aspects/' + ENTITY + '?specific=1'
    )
    assert resp.status_code == 200

    aspects = json.loads(resp.text)
    result_set = _convert_aspects_to_v1_format(aspects)

    assert result_set == SPECIFIC_BASIC_ASPECTS


def _convert_aspects_to_v1_format(aspects):
    v1_format_set = set()
    for a in aspects:
        arg1 = a['arg1'] if a['arg1'] != '.' else '?x'
        arg2 = a['arg2'] if a['arg2'] != '.' else '?x'
        v1_aspect_str = '[{} {} {}]'.format(arg1, a['relation'], arg2)
        v1_format_set.add(v1_aspect_str)

    return v1_format_set
